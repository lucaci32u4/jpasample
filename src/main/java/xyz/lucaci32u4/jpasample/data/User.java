package xyz.lucaci32u4.jpasample.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@Accessors(chain = true)
@NoArgsConstructor
public class User {

    @Id @Getter
    private String id;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private boolean active;

    @Getter
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Message> messages;

    public User(String id) {
        this.id = id;
        messages = new HashSet<>();
    }

    public User addMessage(Message message) {
        messages.add(message);
        message.setUser(this);
        return this;
    }

    public User removeMessage(Message message) {
        messages.remove(message);
        message.setUser(null);
        return this;
    }
}
