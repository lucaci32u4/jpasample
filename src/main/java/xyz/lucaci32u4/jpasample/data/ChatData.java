package xyz.lucaci32u4.jpasample.data;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.ServiceRegistry;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Stream;

public class ChatData {

    private final Session session;
    private Transaction transaction;

    public ChatData(String dbPath, boolean openExplorer)  {
        Configuration cfg = new Configuration()
                .addAnnotatedClass(User.class)
                .addAnnotatedClass(Message.class)
                .addAnnotatedClass(Emote.class);
        ServiceRegistry sevReg = new StandardServiceRegistryBuilder()
                .applySettings(hibernatePropsH2("jdbc:h2:" + dbPath))
                .build();
        SessionFactory sessionFactory = cfg.buildSessionFactory(sevReg);
        session = sessionFactory.openSession();
        if (openExplorer) {
            new Thread(() -> {
                try {
                    org.h2.tools.Server.startWebServer(sevReg.getService(ConnectionProvider.class).getConnection());
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }).start();
        }
    }

    public void saveEntity(Object obj) {
        if (obj instanceof Emote || obj instanceof Message || obj instanceof User) {
            autoTransaction(() -> session.save(obj));
        }
    }

    public <T> Optional<T> findById(Class<T> clazz, String id) {
        return Optional.ofNullable(session.find(clazz, id));
    }

    public <T> Stream<T> findAll(Class<T> clazz) {
        CriteriaQuery<T> cq = session.getCriteriaBuilder().createQuery(clazz);
        return session.createQuery(cq.select(cq.from(clazz))).getResultStream();
    }

    public Stream<User> findUsersByName(String name) {
        TypedQuery<User> query = session.createQuery("SELECT user FROM User AS user WHERE user.name = ?1");
        query.setParameter(1, name);
        return query.getResultStream();
    }

    public Stream<Message> findMessagesWithWord(String word) {
        TypedQuery<Message> query = session.createQuery("SELECT mess FROM Message AS mess WHERE mess.content LIKE ?1");
        query.setParameter(1, word);
        return query.getResultStream();
    }

    public Optional<Emote> findEmoteByRepresentation(String repr) {
        TypedQuery<Emote> query = session.createQuery("SELECT em FROM Emote AS em WHERE em.representation = ?1");
        query.setParameter(1, repr);
        return Optional.ofNullable(query.getSingleResult());
    }

    public <T> Optional<T> deleteEntiy(Class<T> clazz, String id) {
        T ent = session.find(clazz, id);
        if (ent != null) {
            autoTransaction(() -> session.delete(ent));
            return Optional.of(ent);
        } else return Optional.empty();
    }

    public void openTransaction() {
        if (transaction == null) {
            transaction = session.beginTransaction();
        }
    }

    public void closeTransaction() {
        if (transaction != null) {
            transaction.commit();
            transaction = null;
        }
        clearCachedEntities();
    }

    public void clearCachedEntities() {
        session.clear();
    }

    private void autoTransaction(Runnable action) {
        if (transaction == null) {
            openTransaction();
            action.run();
            closeTransaction();
        } else action.run();
    }

    public void close() {
        closeTransaction();
        session.close();
    }

    private static Properties hibernatePropsH2(String h2url) {
        Properties hProps = new Properties();
        hProps.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        hProps.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        hProps.setProperty("hibernate.connection.url", h2url);
        hProps.setProperty("hibernate.connection.username", "sa");
        hProps.setProperty("hibernate.connection.password", "");
        hProps.setProperty("hibernate.hbm2ddl.auto", "update");
        hProps.setProperty("hibernate.show_sql", "false");
        return hProps;
    }

}
