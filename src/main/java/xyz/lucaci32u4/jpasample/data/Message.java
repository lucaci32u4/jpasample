package xyz.lucaci32u4.jpasample.data;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Accessors(chain = true)
@NoArgsConstructor
public class Message {

    @Id @Getter
    private String id;

    @Getter @Setter
    private String content;

    @Getter @Setter(AccessLevel.MODULE)
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user") // join column name
    private User user;

    @Getter
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "messages_emotes",
            joinColumns = @JoinColumn(name = "emote"),
            inverseJoinColumns = @JoinColumn(name = "message"))
    private List<Emote> emotes;

    public Message(String id) {
        this.id = id;
        this.emotes = new ArrayList<>();
    }

    public Message addEmote(Emote emote) {
        emotes.add(emote);
        emote.getMessages().add(this);
        return this;
    }

    public Message removeEmote(Emote emote) {
        emotes.remove(emote);
        emote.getMessages().remove(this);
        return this;
    }
}
