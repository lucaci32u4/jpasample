package xyz.lucaci32u4.jpasample.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Accessors(chain = true)
@NoArgsConstructor
public class Emote {

    @Id @Getter
    private String id;

    @Getter @Setter
    private String representation;

    @Getter
    @ManyToMany(mappedBy = "emotes", cascade = CascadeType.ALL)
    private Set<Message> messages;

    public Emote(String id) {
        this.id = id;
        messages = new HashSet<>();
    }

}
