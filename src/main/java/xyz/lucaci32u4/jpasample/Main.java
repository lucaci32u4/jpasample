package xyz.lucaci32u4.jpasample;

import xyz.lucaci32u4.jpasample.data.ChatData;
import xyz.lucaci32u4.jpasample.data.Emote;
import xyz.lucaci32u4.jpasample.data.Message;
import xyz.lucaci32u4.jpasample.data.User;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        ChatData data = new ChatData("./database", false);
        Scanner in = new Scanner(System.in);

        boolean running = true;
        while (running) {
            String command = in.next("[\\w\\-_]+");
            switch (command.trim().toLowerCase()) {
                case "add-users" -> {
                    User john = new User(UUID.randomUUID().toString());
                    User foo = new User(UUID.randomUUID().toString());
                    User bar = new User(UUID.randomUUID().toString());
                    john.setName("John").setActive(true);
                    foo.setName("Foo").setActive(true);
                    bar.setName("Bar").setActive(true);

                    data.openTransaction();
                    data.saveEntity(john);
                    data.saveEntity(foo);
                    data.saveEntity(bar);
                    data.closeTransaction();
                }
                case "add-emotes" -> {
                    data.openTransaction();
                    Stream.of(":happy:", ":sad:", ":confused:", ":meow:", ":laugh:", ":like:", ":dislike:")
                            .map(r -> new Emote(UUID.randomUUID().toString()).setRepresentation(r))
                            .forEach(data::saveEntity);
                    data.closeTransaction();
                }
                case "set-inactive" -> {
                    data.openTransaction();
                    Stream.of("John", "Booo").forEach(uname -> {
                        Optional<User> user = data.findUsersByName(uname).findAny();
                        if (user.isPresent()) {
                            user.get().setActive(false);
                            System.out.println("User " + uname + " is now inactive");
                        } else {
                            System.out.println("No such user: " + uname);
                        }
                    });
                    data.closeTransaction();
                }
                case "add-message" -> {
                    Message fooMsg = new Message(UUID.randomUUID().toString());
                    Message barMsg = new Message(UUID.randomUUID().toString());
                    fooMsg.setContent("Hey how are you?");
                    barMsg.setContent("Fine, you?");

                    data.openTransaction();
                    data.findUsersByName("Foo").findAny().ifPresent(u -> u.addMessage(fooMsg));
                    data.findUsersByName("Bar").findAny().ifPresent(u -> u.addMessage(barMsg));
                    data.closeTransaction();
                }
                case "emote-message" -> {
                    Emote happy = data.findEmoteByRepresentation(":happy:").orElse(null);
                    Emote like = data.findEmoteByRepresentation(":like:").orElse(null);
                    Emote meow = data.findEmoteByRepresentation(":meow:").orElse(null);

                    data.openTransaction();
                    data.findUsersByName("Foo").findAny().flatMap(u -> u.getMessages()
                            .stream().findAny()).ifPresent(m -> m.addEmote(happy));
                    data.findUsersByName("Bar").findAny().flatMap(u -> u.getMessages()
                            .stream().findAny()).ifPresent(m -> m.addEmote(like).addEmote(meow));
                    data.closeTransaction();
                }
                case "print-message" -> {
                    String printStr = data.findAll(Message.class)
                            .map(m -> m.getUser().getName()
                                    + ": " + m.getContent()
                                    + "\n  => "
                                    + m.getEmotes().stream().map(Emote::getRepresentation).collect(Collectors.joining(", ")))
                            .collect(Collectors.joining("\n"));
                    System.out.println(printStr);
                    data.clearCachedEntities();
                }
                case "exit", "quit" -> running = false;
                default -> System.out.println("No such command");
            }
        }
        data.close();

    }
}
